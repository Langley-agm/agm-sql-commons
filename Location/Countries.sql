/*

	Countries table query.

	You can run it as much as you can, it will not replicate tables or records.

	Author: Armando Garcia Moran
	Tested in MySQL 5.6
	Data obtained from: http://countrycode.org/

*/

/* Create the table if it doesn't exist yet. */
CREATE TABLE IF NOT EXISTS countries (
	`id` SMALLINT NOT NULL AUTO_INCREMENT PRIMARY KEY,
	`name` varchar(128) NOT NULL UNIQUE,
	`code` smallint,
	`isocode2` varchar(2),
	`isocode3` varchar(3)
);

/* Create all countries in just one Insert unless they have already been created. */
INSERT IGNORE INTO countries( name, code, isocode2, isocode3 ) 
SELECT 'Afghanistan', 93, 'AF', 'AFG' UNION ALL
SELECT 'Albania', 355, 'AL', 'ALB' UNION ALL
SELECT 'Algeria', 213 , 'DZ', 'DZA' UNION ALL
SELECT 'American Samoa', 1684 , 'AS', 'ASM' UNION ALL
SELECT 'Andorra', 376, 'AD', 'AND' UNION ALL
SELECT 'Angola', 244, 'AO', 'AGO' UNION ALL
SELECT 'Anguilla', 1264, 'AI', 'AIA' UNION ALL
SELECT 'Antarctica', 672, 'AQ', 'ATA' UNION ALL
SELECT 'Antigua and Barbuda', 1268, 'AG', 'ATG' UNION ALL
SELECT 'Argentina', 54, 'AR', 'ARG' UNION ALL
SELECT 'Armenia', 374, 'AM', 'ARM' UNION ALL
SELECT 'Aruba', 297, 'AW', 'ABW' UNION ALL
SELECT 'Australia', 61, 'AU', 'AUS' UNION ALL
SELECT 'Austria', 43, 'AT', 'AUT' UNION ALL
SELECT 'Azerbaijan', 994, 'AZ', 'AZE' UNION ALL
SELECT 'Bahamas', 1242, 'BS', 'BHS' UNION ALL
SELECT 'Bahrain', 973, 'BH', 'BHR' UNION ALL
SELECT 'Bangladesh', 880, 'BD', 'BGD' UNION ALL
SELECT 'Barbados', 1246, 'BB', 'BRB' UNION ALL
SELECT 'Belarus', 375, 'BY', 'BLR' UNION ALL
SELECT 'Belgium', 32, 'BE', 'BEL' UNION ALL
SELECT 'Belize', 501, 'BZ', 'BLZ' UNION ALL
SELECT 'Benin', 229, 'BJ', 'BEN' UNION ALL
SELECT 'Bermuda', 1441, 'BM', 'BMU' UNION ALL
SELECT 'Bhutan', 975, 'BT', 'BTN' UNION ALL
SELECT 'Bolivia', 591, 'BO', 'BOL' UNION ALL
SELECT 'Bosnia and Herzegovina', 387, 'BA', 'BIH' UNION ALL
SELECT 'Botswana', 267, 'BW', 'BWA' UNION ALL
SELECT 'Brazil', 55, 'BR', 'BRA' UNION ALL
SELECT 'British Indian Ocean Territory', 0, 'IO', 'IOT' UNION ALL
SELECT 'British Virgin Islands', 1284, 'VG', 'VGB' UNION ALL
SELECT 'Brunei', 673, 'BN', 'BRN' UNION ALL
SELECT 'Bulgaria', 359, 'BG', 'BGR' UNION ALL
SELECT 'Burkina Faso', 226, 'BF', 'BFA' UNION ALL
SELECT 'Burma (Myanmar)', 95, 'MM', 'MMR' UNION ALL
SELECT 'Burundi', 257, 'BI', 'BDI' UNION ALL
SELECT 'Cambodia', 855, 'KH', 'KHM' UNION ALL
SELECT 'Cameroon', 237, 'CM', 'CMR' UNION ALL
SELECT 'Canada', 1, 'CA', 'CAN' UNION ALL
SELECT 'Cape Verde', 238, 'CV', 'CPV' UNION ALL
SELECT 'Cayman Islands', 1345, 'KY', 'CYM' UNION ALL
SELECT 'Central African Republic', 236, 'CF', 'CAF' UNION ALL
SELECT 'Chad', 235, 'TD', 'TCD' UNION ALL
SELECT 'Chile', 56, 'CL', 'CHL' UNION ALL
SELECT 'China', 86, 'CN', 'CHN' UNION ALL
SELECT 'Christmas Island', 61, 'CX', 'CXR' UNION ALL
SELECT 'Cocos (Keeling) Islands', 61, 'CC', 'CCK' UNION ALL
SELECT 'Colombia', 57, 'CO', 'COL' UNION ALL
SELECT 'Comoros', 269, 'KM', 'COM' UNION ALL
SELECT 'Cook Islands', 682, 'CK', 'COK' UNION ALL
SELECT 'Costa Rica', 506, 'CR', 'CRC' UNION ALL
SELECT 'Croatia', 385, 'HR', 'HRV' UNION ALL
SELECT 'Cuba', 53, 'CU', 'CUB' UNION ALL
SELECT 'Cyprus', 357, 'CY', 'CYP' UNION ALL
SELECT 'Czech Republic', 420, 'CZ', 'CZE' UNION ALL
SELECT 'Democratic Republic of the Congo', 243, 'CD', 'COD' UNION ALL
SELECT 'Denmark', 45, 'DK', 'DNK' UNION ALL
SELECT 'Djibouti', 253, 'DJ', 'DJI' UNION ALL
SELECT 'Dominica', 1767, 'DM', 'DMA' UNION ALL
SELECT 'Dominican Republic', 1809, 'DO', 'DOM' UNION ALL
SELECT 'Ecuador', 593, 'EC', 'ECU' UNION ALL
SELECT 'Egypt', 20, 'EG', 'EGY' UNION ALL
SELECT 'El Salvador', 503, 'SV', 'SLV' UNION ALL
SELECT 'Equatorial Guinea', 240, 'GQ', 'GNQ' UNION ALL
SELECT 'Eritrea', 291, 'ER', 'ERI' UNION ALL
SELECT 'Estonia', 372, 'EE', 'EST' UNION ALL
SELECT 'Ethiopia', 251, 'ET', 'ETH' UNION ALL
SELECT 'Falkland Islands', 500, 'FK', 'FLK' UNION ALL
SELECT 'Faroe Islands', 298, 'FO', 'FRO' UNION ALL
SELECT 'Fiji', 679, 'FJ', 'FJI' UNION ALL
SELECT 'Finland', 358, 'FI', 'FIN' UNION ALL
SELECT 'France', 33, 'FR', 'FRA' UNION ALL
SELECT 'French Polynesia', 689, 'PF', 'PYF' UNION ALL
SELECT 'Gabon', 241, 'GA', 'GAB' UNION ALL
SELECT 'Gambia', 220, 'GM', 'GMB' UNION ALL
SELECT 'Gaza Strip', 970, NULL, NULL UNION ALL
SELECT 'Georgia', 995, 'GE', 'GEO' UNION ALL
SELECT 'Germany', 49, 'DE', 'DEU' UNION ALL
SELECT 'Ghana', 233, 'GH', 'GHA' UNION ALL
SELECT 'Gibraltar', 350, 'GI', 'GIB' UNION ALL
SELECT 'Greece', 30, 'GR', 'GRC' UNION ALL
SELECT 'Greenland', 299, 'GL', 'GRL' UNION ALL
SELECT 'Grenada', 1473, 'GD', 'GRD' UNION ALL
SELECT 'Guam', 1671, 'GU', 'GUM' UNION ALL
SELECT 'Guatemala', 502, 'GT', 'GTM' UNION ALL
SELECT 'Guinea', 224, 'GN', 'GIN' UNION ALL
SELECT 'Guinea-Bissau', 245, 'GW', 'GNB' UNION ALL
SELECT 'Guyana', 592, 'GY', 'GUY' UNION ALL
SELECT 'Haiti', 509, 'HT', 'HTI' UNION ALL
SELECT 'Holy See (Vatican City)', 39, 'VA', 'VAT' UNION ALL
SELECT 'Honduras', 504, 'HN', 'HND' UNION ALL
SELECT 'Hong Kong', 852, 'HK', 'HKG' UNION ALL
SELECT 'Hungary', 36, 'HU', 'HUN' UNION ALL
SELECT 'Iceland', 354, 'IS', 'IS' UNION ALL
SELECT 'India', 91, 'IN', 'IND' UNION ALL
SELECT 'Indonesia', 62, 'ID', 'IDN' UNION ALL
SELECT 'Iran', 98, 'IR', 'IRN' UNION ALL
SELECT 'Iraq', 964, 'IQ', 'IRQ' UNION ALL
SELECT 'Ireland', 353, 'IE', 'IRL' UNION ALL
SELECT 'Isle of Man', 44, 'IM', 'IMN' UNION ALL
SELECT 'Israel', 972, 'IL', 'ISR' UNION ALL
SELECT 'Italy', 39, 'IT', 'ITA' UNION ALL
SELECT 'Ivory Coast', 225, 'CI', 'CIV' UNION ALL
SELECT 'Jamaica', 1876, 'JM', 'JAM' UNION ALL
SELECT 'Japan', 81, 'JP', 'JPN' UNION ALL
SELECT 'Jersey', NULL, 'JE', 'JEY' UNION ALL
SELECT 'Jordan', 962, 'JO', 'JOR' UNION ALL
SELECT 'Kazakhstan', 7, 'KZ', 'KAZ' UNION ALL
SELECT 'Kenya', 254, 'KE', 'KEN' UNION ALL
SELECT 'Kiribati', 686, 'KI', 'KIR' UNION ALL
SELECT 'Kosovo', 381, NULL, NULL UNION ALL
SELECT 'Kuwait', 965, 'KW', 'KWT' UNION ALL
SELECT 'Kyrgyzstan', 996, 'KG', 'KGZ' UNION ALL
SELECT 'Laos', 856, 'LA', 'LAO' UNION ALL
SELECT 'Latvia', 371, 'LV', 'LVA' UNION ALL
SELECT 'Lebanon', 961, 'LB', 'LBN' UNION ALL
SELECT 'Lesotho', 266, 'LS', 'LSO' UNION ALL
SELECT 'Liberia', 231, 'LR', 'LBR' UNION ALL
SELECT 'Libya', 218, 'LY', 'LBY' UNION ALL
SELECT 'Liechtenstein', 423, 'LI', 'LIE' UNION ALL
SELECT 'Lithuania', 370, 'LT', 'LTU' UNION ALL
SELECT 'Luxembourg', 352, 'LU', 'LUX' UNION ALL
SELECT 'Macau', 853, 'MO', 'MAC' UNION ALL
SELECT 'Macedonia', 389, 'MK', 'MKD' UNION ALL
SELECT 'Madagascar', 261, 'MG', 'MDG' UNION ALL
SELECT 'Malawi', 265, 'MW', 'MWI' UNION ALL
SELECT 'Malaysia', 60, 'MY', 'MYS' UNION ALL
SELECT 'Maldives', 960, 'MV', 'MDV' UNION ALL
SELECT 'Mali', 223, 'ML', 'MLI' UNION ALL
SELECT 'Malta', 356, 'MT', 'MLT' UNION ALL
SELECT 'Marshall Islands', 692, 'MH', 'MHL' UNION ALL
SELECT 'Mauritania', 222, 'MR', 'MRT' UNION ALL
SELECT 'Mauritius', 230, 'MU', 'MUS' UNION ALL
SELECT 'Mayotte', 262, 'YT', 'MYT' UNION ALL
SELECT 'Mexico', 52, 'MX', 'MEX' UNION ALL
SELECT 'Micronesia', 691, 'FM', 'FSM' UNION ALL
SELECT 'Moldova', 373, 'MD', 'MDA' UNION ALL
SELECT 'Monaco', 377, 'MC', 'MCO' UNION ALL
SELECT 'Mongolia', 976, 'MN', 'MNG' UNION ALL
SELECT 'Montenegro', 382, 'ME', 'MNE' UNION ALL
SELECT 'Montserrat', 1664, 'MS', 'MSR' UNION ALL
SELECT 'Morocco', 212, 'MA', 'MAR' UNION ALL
SELECT 'Mozambique', 258, 'MZ', 'MOZ' UNION ALL
SELECT 'Namibia', 264, 'NA', 'NAM' UNION ALL
SELECT 'Nauru', 674, 'NR', 'NRU' UNION ALL
SELECT 'Nepal', 977, 'NP', 'NPL' UNION ALL
SELECT 'Netherlands', 31, 'NL', 'NLD' UNION ALL
SELECT 'Netherlands Antilles', 599, 'AN', 'ANT' UNION ALL
SELECT 'New Caledonia', 687, 'NC', 'NCL' UNION ALL
SELECT 'New Zealand', 64, 'NZ', 'NZL' UNION ALL
SELECT 'Nicaragua', 505, 'NI', 'NIC' UNION ALL
SELECT 'Niger', 227, 'NE', 'NER' UNION ALL
SELECT 'Nigeria', 234, 'NG', 'NGA' UNION ALL
SELECT 'Niue', 683, 'NU', 'NIU' UNION ALL
SELECT 'Norfolk Island', 672, NULL, 'NFK' UNION ALL
SELECT 'North Korea', 850, 'KP', 'PRK' UNION ALL
SELECT 'Northern Mariana Islands', 1670, 'MP', 'MNP' UNION ALL
SELECT 'Norway', 47, 'NO', 'NOR' UNION ALL
SELECT 'Oman', 968, 'OM', 'OMN' UNION ALL
SELECT 'Pakistan', 92, 'PK', 'PAK' UNION ALL
SELECT 'Palau', 680, 'PW', 'PLW' UNION ALL
SELECT 'Panama', 507, 'PA', 'PAN' UNION ALL
SELECT 'Papua New Guinea', 675, 'PG', 'PNG' UNION ALL
SELECT 'Paraguay', 595, 'PY', 'PRY' UNION ALL
SELECT 'Peru', 51, 'PE', 'PER' UNION ALL
SELECT 'Philippines', 63, 'PH', 'PHL' UNION ALL
SELECT 'Pitcairn Islands', 870, 'PN', 'PCN' UNION ALL
SELECT 'Poland', 48, 'PL', 'POL' UNION ALL
SELECT 'Portugal', 351, 'PT', 'PRT' UNION ALL
SELECT 'Puerto Rico', 1, 'PR', 'PRI' UNION ALL
SELECT 'Qatar', 974, 'QA', 'QAT' UNION ALL
SELECT 'Republic of the Congo', 242, 'CG', 'COG' UNION ALL
SELECT 'Romania', 40, 'RO', 'ROU' UNION ALL
SELECT 'Russia', 7, 'RU', 'RUS' UNION ALL
SELECT 'Rwanda', 250, 'RW', 'RWA' UNION ALL
SELECT 'Saint Barthelemy', 590, 'BL', 'BLM' UNION ALL
SELECT 'Saint Helena', 290, 'SH', 'SHN' UNION ALL
SELECT 'Saint Kitts and Nevis', 1869, 'KN', 'KNA' UNION ALL
SELECT 'Saint Lucia', 1758, 'LC', 'LCA' UNION ALL
SELECT 'Saint Martin', 1599, 'MF', 'MAF' UNION ALL
SELECT 'Saint Pierre and Miquelon', 508, 'PM', 'SPM' UNION ALL
SELECT 'Saint Vincent and the Grenadines', 1784, 'VC', 'VCT' UNION ALL
SELECT 'Samoa', 685, 'WS', 'WSM' UNION ALL
SELECT 'San Marino', 378, 'SM', 'SMR' UNION ALL
SELECT 'Sao Tome and Principe', 239, 'ST', 'STP' UNION ALL
SELECT 'Saudi Arabia', 966, 'SA', 'SAU' UNION ALL
SELECT 'Senegal', 221, 'SN', 'SEN' UNION ALL
SELECT 'Serbia', 381, 'RS', 'SRB' UNION ALL
SELECT 'Seychelles', 248, 'SC', 'SYC' UNION ALL
SELECT 'Sierra Leone', 232, 'SL', 'SLE' UNION ALL
SELECT 'Singapore', 65, 'SG', 'SGP' UNION ALL
SELECT 'Slovakia', 421, 'SK', 'SVK' UNION ALL
SELECT 'Slovenia', 386, 'SI', 'SVN' UNION ALL
SELECT 'Solomon Islands', 677, 'SB', 'SLB' UNION ALL
SELECT 'Somalia', 252, 'SO', 'SOM' UNION ALL
SELECT 'South Africa', 27, 'ZA', 'ZAF' UNION ALL
SELECT 'South Korea', 82, 'KR', 'KOR' UNION ALL
SELECT 'Spain', 34, 'ES', 'ESP' UNION ALL
SELECT 'Sri Lanka', 94, 'LK', 'LKA' UNION ALL
SELECT 'Sudan', 249, 'SD', 'SDN' UNION ALL
SELECT 'Suriname', 597, 'SR', 'SUR' UNION ALL
SELECT 'Svalbard', NULL, 'SJ', 'SJM' UNION ALL
SELECT 'Swaziland', 268, 'SZ', 'SWZ' UNION ALL
SELECT 'Sweden', 46, 'SE', 'SWE' UNION ALL
SELECT 'Switzerland', 41, 'CH', 'CHE' UNION ALL
SELECT 'Syria', 963, 'SY', 'SYR' UNION ALL
SELECT 'Taiwan', 886, 'TW', 'TWN' UNION ALL
SELECT 'Tajikistan', 992, 'TJ', 'TJK' UNION ALL
SELECT 'Tanzania', 255, 'TZ', 'TZA' UNION ALL
SELECT 'Thailand', 66, 'TH', 'THA' UNION ALL
SELECT 'Timor-Leste', 670, 'TL', 'TLS' UNION ALL
SELECT 'Togo', 228, 'TG', 'TGO' UNION ALL
SELECT 'Tokelau', 690, 'TK', 'TKL' UNION ALL
SELECT 'Tonga', 676, 'TO', 'TON' UNION ALL
SELECT 'Trinidad and Tobago', 1868, 'TT', 'TTO' UNION ALL
SELECT 'Tunisia', 216, 'TN', 'TUN' UNION ALL
SELECT 'Turkey', 90, 'TR', 'TUR' UNION ALL
SELECT 'Turkmenistan', 993, 'TM', 'TKM' UNION ALL
SELECT 'Turks and Caicos Islands', 1649, 'TC', 'TCA' UNION ALL
SELECT 'Tuvalu', 688, 'TV', 'TUV' UNION ALL
SELECT 'Uganda', 256, 'UG', 'UGA' UNION ALL
SELECT 'Ukraine', 380, 'UA', 'UKR' UNION ALL
SELECT 'United Arab Emirates', 971, 'AE', 'ARE' UNION ALL
SELECT 'United Kingdom', 44, 'GB', 'GBR' UNION ALL
SELECT 'United States', 1, 'US', 'USA' UNION ALL
SELECT 'Uruguay', 598, 'UY', 'URY' UNION ALL
SELECT 'US Virgin Islands', 1340, 'VI', 'VIR' UNION ALL
SELECT 'Uzbekistan', 998, 'UZ', 'UZB' UNION ALL
SELECT 'Vanuatu', 678, 'VU', 'VUT' UNION ALL
SELECT 'Venezuela', 58, 'VE', 'VEN' UNION ALL
SELECT 'Vietnam', 84, 'VN', 'VNM' UNION ALL
SELECT 'Wallis and Futuna', 681, 'WF', 'WLF' UNION ALL
SELECT 'West Bank', 970, NULL, NULL UNION ALL
SELECT 'Western Sahara', NULL, 'EH', 'ESH' UNION ALL
SELECT 'Yemen', 967, 'YE', 'YEM' UNION ALL
SELECT 'Zambia', 260, 'ZM', 'ZMB' UNION ALL
SELECT 'Zimbabwe', 263, 'ZW', 'ZWE';

/* Select all countries to visually review them. */
SELECT * FROM countries;

/*

Related Queries:

DROP TABLE countries;
DELETE FROM countries;

*/