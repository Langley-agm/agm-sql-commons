# Welcome

I often find that I have need for very common queries and creating them is time consuming. I usually try to find them online so you know... I don't reinvent the wheel, but usually I don't find one that completely follows the standards and quality I would use, so I decided to just add them here. Feel free to use them if they are of use to you and if you think there's improvement to them make sure to let me know or create a pull request so your work gets added to this project.

## Countries Query.

Advantages and differences with other queries out there:

* This query doesn't specify id for any record nor should you, I don't know why every query I found out there does it.
* You can execute it many times and it will create the table and the registers only once.
* The countries insertion gets done with only 1 insert sentence, which makes it faster.
* Has a complete list of countries from this source: http://countrycode.org/ , including country code which is useful when dealing with phone input in forms.
* It only has the country names in one language, IMHO you should be translating from presentation or dictionaries, not from the database.
* Includes 2 different types of country codes from two different ISO specifications, you can use whichever you want and even delete the other if you don't need it.

This is an example of the query:

```
#!sql

CREATE TABLE IF NOT EXISTS countries (
	`id` SMALLINT NOT NULL AUTO_INCREMENT PRIMARY KEY,
	`name` varchar(128) NOT NULL UNIQUE,
	`code` smallint,
	`isocode2` varchar(2),
	`isocode3` varchar(3)
);

/* Create all countries in just one Insert unless they have already been created. */
INSERT IGNORE INTO countries( name, code, isocode2, isocode3 ) 
SELECT 'Afghanistan', 93, 'AF', 'AFG' UNION ALL
SELECT 'Albania', 355, 'AL', 'ALB' UNION ALL
SELECT 'Algeria', 213 , 'DZ', 'DZA' UNION ALL
...

```

You can get the whole sql file from: [Countries.sql](https://bitbucket.org/ArmandoGarcia/agm-sql-commons/annotate/686bc3ddcaa6c472261b3cd27fe52fb474140599/Location/Countries.sql?at=master)
